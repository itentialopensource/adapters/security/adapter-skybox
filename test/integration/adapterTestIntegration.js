/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-skybox',
      type: 'Skybox',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Skybox = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Skybox Adapter Test', () => {
  describe('Skybox Class Tests', () => {
    const a = new Skybox(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#get - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.get((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.company);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.phone);
                assert.equal('string', data.response.externalMerchantId);
                assert.equal('string', data.response.creationDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.address1);
                assert.equal('string', data.response.address2);
                assert.equal('string', data.response.city);
                assert.equal('AS', data.response.state);
                assert.equal('string', data.response.postalCode);
                assert.equal('NL', data.response.country);
                assert.equal('string', data.response.logo);
                assert.equal(true, data.response.nativeIntegrationEnabled);
                assert.equal(false, data.response.optInStf);
                assert.equal(false, data.response.deleteCancelledTickets);
                assert.equal(true, Array.isArray(data.response.accountSettings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'get', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNotifications((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'getNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let addressesAddressId = 555;
    const addressesInsertBodyParam = {
      address1: 'string',
      city: 'string',
      state: 'NS',
      postalCode: 'string',
      country: 'string',
      phone1: 'string'
    };
    describe('#insert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insert(addressesInsertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal(3, data.response.accountId);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.company);
                assert.equal('string', data.response.address1);
                assert.equal('string', data.response.address2);
                assert.equal('string', data.response.city);
                assert.equal('NB', data.response.state);
                assert.equal('string', data.response.postalCode);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.phone1);
                assert.equal('string', data.response.phone2);
                assert.equal('string', data.response.fax);
              } else {
                runCommonAsserts(data, error);
              }
              addressesAddressId = data.response.id;
              saveMockData('Addresses', 'insert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressesUpdateBodyParam = {
      address1: 'string',
      city: 'string',
      state: 'AR',
      postalCode: 'string',
      country: 'string',
      phone1: 'string'
    };
    describe('#update - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update(addressesAddressId, addressesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addresses', 'update', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById(addressesAddressId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(4, data.response.accountId);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.company);
                assert.equal('string', data.response.address1);
                assert.equal('string', data.response.address2);
                assert.equal('string', data.response.city);
                assert.equal('MD', data.response.state);
                assert.equal('string', data.response.postalCode);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.phone1);
                assert.equal('string', data.response.phone2);
                assert.equal('string', data.response.fax);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addresses', 'getById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getByAccount((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getByAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAlertId = 555;
    describe('#getById1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById1(alertsAlertId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal(8, data.response.creatorId);
                assert.equal(5, data.response.accountId);
                assert.equal('string', data.response.subject);
                assert.equal('string', data.response.message);
                assert.equal('RELEASE', data.response.alertType);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal(true, data.response.acknowledged);
                assert.equal('string', data.response.senderName);
                assert.equal('string', data.response.senderEmail);
                assert.equal(2, data.response.releaseAlertId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getById1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const assetsUploadBodyParam = {
      pdf: [
        'string'
      ]
    };
    describe('#upload - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.upload(assetsUploadBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Assets', 'upload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const assetsFilename = 'fakedata';
    describe('#get1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.get1(assetsFilename, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Assets', 'get1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditAuditEntity = 'fakedata';
    const auditTargetId = 555;
    describe('#search - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search(auditAuditEntity, null, auditTargetId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'search', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const creditCardsInsert1BodyParam = {
      token: 'string',
      billingAddress: {
        firstName: 'string',
        lastName: 'string',
        email: 'string',
        address1: 'string',
        address2: 'string',
        city: 'string',
        state: 'OTHER',
        postalCode: 'string',
        phone: 'string',
        country: 'string',
        persistInVault: false
      },
      cardholderName: 'string',
      defaultCard: true
    };
    describe('#insert1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insert1(creditCardsInsert1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.token);
                assert.equal('object', typeof data.response.billingAddress);
                assert.equal('string', data.response.cardholderName);
                assert.equal(false, data.response.defaultCard);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'insert1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let creditCardsId = 'fakedata';
    const creditCardsInsertGroupBodyParam = {
      id: 6,
      accountId: 8,
      label: 'string',
      description: 'string',
      creditCards: [
        {
          token: 'string',
          billingAddress: {
            firstName: 'string',
            lastName: 'string',
            email: 'string',
            address1: 'string',
            address2: 'string',
            city: 'string',
            state: 'CO',
            postalCode: 'string',
            phone: 'string',
            country: 'string',
            persistInVault: true
          },
          cardholderName: 'string',
          defaultCard: true
        }
      ]
    };
    describe('#insertGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertGroup(creditCardsInsertGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal(10, data.response.accountId);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.creditCards));
              } else {
                runCommonAsserts(data, error);
              }
              creditCardsId = data.response.id;
              saveMockData('CreditCards', 'insertGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const creditCardsUpdate1BodyParam = {
      token: 'string',
      billingAddress: {
        firstName: 'string',
        lastName: 'string',
        email: 'string',
        address1: 'string',
        address2: 'string',
        city: 'string',
        state: 'NJ',
        postalCode: 'string',
        phone: 'string',
        country: 'string',
        persistInVault: true
      },
      cardholderName: 'string',
      defaultCard: false
    };
    describe('#update1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update1(creditCardsUpdate1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'update1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCreditCardsByAccountId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCreditCardsByAccountId((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'getCreditCardsByAccountId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const creditCardsUpdateGroupBodyParam = {
      id: 2,
      accountId: 4,
      label: 'string',
      description: 'string',
      creditCards: [
        {
          token: 'string',
          billingAddress: {
            firstName: 'string',
            lastName: 'string',
            email: 'string',
            address1: 'string',
            address2: 'string',
            city: 'string',
            state: 'WA',
            postalCode: 'string',
            phone: 'string',
            country: 'string',
            persistInVault: true
          },
          cardholderName: 'string',
          defaultCard: true
        }
      ]
    };
    describe('#updateGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroup(creditCardsUpdateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'updateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#query - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.query(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'query', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const creditCardsGroupId = 555;
    describe('#getCreditCards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCreditCards(creditCardsGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'getCreditCards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupById(creditCardsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal(2, data.response.accountId);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.creditCards));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'getGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById2(creditCardsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.token);
                assert.equal('object', typeof data.response.billingAddress);
                assert.equal('string', data.response.cardholderName);
                assert.equal(true, data.response.defaultCard);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'getById2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let customersCustomerId = 555;
    let customersCardId = 555;
    const customersInsert2BodyParam = {
      accountId: 10,
      email: 'string',
      address: {
        accountId: 2
      },
      displayName: 'string',
      customerType: 'BROKER',
      salesTerm: 'NET60',
      paymentMethod: 'OTHER',
      defaultDeliveryMethod: 'MOBILE_DELIVERY'
    };
    describe('#insert2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insert2(customersInsert2BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(4, data.response.accountId);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastInvoiceDate);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.email2);
                assert.equal(false, data.response.useEmail2);
                assert.equal('string', data.response.company);
                assert.equal('object', typeof data.response.address);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.outlawedPaymentMethods));
                assert.equal('string', data.response.displayName);
                assert.equal('BROKER', data.response.customerType);
                assert.equal('NET30', data.response.salesTerm);
                assert.equal('ACCRUEDCREDIT', data.response.paymentMethod);
                assert.equal(7, data.response.arThreshold);
                assert.equal(true, data.response.deleted);
                assert.equal('string', data.response.tags);
                assert.equal(7, data.response.outstandingBalance);
                assert.equal('MEETANDGREET', data.response.defaultDeliveryMethod);
              } else {
                runCommonAsserts(data, error);
              }
              customersCustomerId = data.response.id;
              customersCardId = data.response.id;
              saveMockData('Customers', 'insert2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersBulkRemoveTagsBodyParam = [
      9
    ];
    describe('#bulkRemoveTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkRemoveTags(customersBulkRemoveTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'bulkRemoveTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersAddCardBodyParam = {
      token: 'string',
      billingAddress: {
        firstName: 'string',
        lastName: 'string',
        email: 'string',
        address1: 'string',
        address2: 'string',
        city: 'string',
        state: 'NS',
        postalCode: 'string',
        phone: 'string',
        country: 'string',
        persistInVault: true
      },
      cardholderName: 'string',
      defaultCard: true
    };
    describe('#addCard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCard(customersCustomerId, customersAddCardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(1, data.response.accountId);
                assert.equal('MASTERCARD', data.response.cardType);
                assert.equal(8, data.response.lastFourDigits);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.customerExternalId);
                assert.equal('string', data.response.addressExternalId);
                assert.equal('string', data.response.cardholderName);
                assert.equal(2, data.response.expirationMonth);
                assert.equal(2, data.response.expirationYear);
                assert.equal(4, data.response.customerId);
                assert.equal(false, data.response.defaultCard);
                assert.equal('string', data.response.lastDateUsed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'addCard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersTagBodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#tag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tag(customersCustomerId, customersTagBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'tag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersDelete1BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete1(customersCustomerId, customersDelete1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'delete1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersTag = [];
    describe('#search1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search1(null, null, null, null, customersTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'search1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersUpdate2BodyParam = {
      customerIds: [
        4
      ]
    };
    describe('#update2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update2(customersUpdate2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'update2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersUpdate3BodyParam = {
      marketplace: 'SCOREBIG'
    };
    describe('#update3 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update3(customersUpdate3BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'update3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultsById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefaultsById((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'getDefaultsById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customersUpdate4BodyParam = {
      accountId: 4,
      email: 'string',
      address: {
        accountId: 3
      },
      displayName: 'string',
      customerType: 'TRAVELAGENT',
      salesTerm: 'NET30',
      paymentMethod: 'EVOPAY',
      defaultDeliveryMethod: 'FLASHSEATS'
    };
    describe('#update4 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update4(customersCustomerId, customersUpdate4BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'update4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById3 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById3(customersCustomerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal(2, data.response.accountId);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastInvoiceDate);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.email2);
                assert.equal(true, data.response.useEmail2);
                assert.equal('string', data.response.company);
                assert.equal('object', typeof data.response.address);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.outlawedPaymentMethods));
                assert.equal('string', data.response.displayName);
                assert.equal('MARKETPLACE', data.response.customerType);
                assert.equal('NET10', data.response.salesTerm);
                assert.equal('ACCRUEDCREDIT', data.response.paymentMethod);
                assert.equal(1, data.response.arThreshold);
                assert.equal(true, data.response.deleted);
                assert.equal('string', data.response.tags);
                assert.equal(4, data.response.outstandingBalance);
                assert.equal('UPS', data.response.defaultDeliveryMethod);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'getById3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCards1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCards1(customersCustomerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'getCards1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCards(customersCustomerId, customersCardId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal(3, data.response.accountId);
                assert.equal('AMEX', data.response.cardType);
                assert.equal(7, data.response.lastFourDigits);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.customerExternalId);
                assert.equal('string', data.response.addressExternalId);
                assert.equal('string', data.response.cardholderName);
                assert.equal(9, data.response.expirationMonth);
                assert.equal(2, data.response.expirationYear);
                assert.equal(5, data.response.customerId);
                assert.equal(false, data.response.defaultCard);
                assert.equal('string', data.response.lastDateUsed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'getCards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultCard - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDefaultCard(customersCustomerId, customersCardId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'setDefaultCard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enumerationsEnumtype = 'fakedata';
    describe('#enumeration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enumeration(enumerationsEnumtype, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enumerations', 'enumeration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventsBulkRemoveTags1BodyParam = [
      4
    ];
    describe('#bulkRemoveTags1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkRemoveTags1(eventsBulkRemoveTags1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'bulkRemoveTags1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventsEventId = 555;
    const eventsTag1BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#tag1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tag1(eventsEventId, eventsTag1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'tag1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventsDelete2BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete2(eventsEventId, eventsDelete2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'delete2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventsPerformerId = 555;
    const eventsTag = [];
    describe('#index - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.index(null, null, null, eventsTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, eventsEventId, eventsPerformerId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'index', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCategories((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'getCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformers(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'getPerformers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformerById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformerById(eventsPerformerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('OTHER', data.response.eventType);
                assert.equal('object', typeof data.response.category);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'getPerformerById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.get2(eventsEventId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.date);
                assert.equal('object', typeof data.response.venue);
                assert.equal(3, data.response.performerId);
                assert.equal('object', typeof data.response.performer);
                assert.equal('string', data.response.keywords);
                assert.equal('string', data.response.chartUrl);
                assert.equal(3, data.response.stubhubEventId);
                assert.equal('string', data.response.stubhubEventUrl);
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.notes);
                assert.equal(9, data.response.eiEventId);
                assert.equal(true, data.response.optOutReplenishment);
                assert.equal(1, data.response.ticketCount);
                assert.equal(1, data.response.mySoldTickets);
                assert.equal(true, data.response.disabled);
                assert.equal('string', data.response.vividSeatsEventUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'get2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#index1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.index1(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventPositions', 'index1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategories1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCategories1((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventPositions', 'getCategories1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformers1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformers1(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventPositions', 'getPerformers1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let externalAccountsId = 'fakedata';
    const externalAccountsCreateBodyParam = {
      displayName: 'string',
      marketplace: 'SCOREBIG',
      integrationProvider: 'NONE'
    };
    describe('#create - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.create(externalAccountsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(5, data.response.accountId);
                assert.equal('string', data.response.displayName);
                assert.equal('SEATGEEK', data.response.marketplace);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.apiKey);
                assert.equal('NATIVE', data.response.integrationProvider);
                assert.equal('string', data.response.lastSweep);
                assert.equal(1, data.response.creditCardId);
                assert.equal(7, data.response.creditCardGroupId);
                assert.equal('string', data.response.encryptedPassword);
                assert.equal(5, data.response.customerId);
                assert.equal(5, data.response.vendorId);
                assert.equal(2, data.response.markup);
                assert.equal(1, data.response.fee);
                assert.equal(true, data.response.autoHideSeatNumbers);
                assert.equal(false, data.response.optInStubhubInhandDateLogic);
                assert.equal(true, data.response.optInStubHubTransferDetailsSweep);
                assert.equal(true, data.response.confirmOrders);
              } else {
                runCommonAsserts(data, error);
              }
              externalAccountsId = data.response.id;
              saveMockData('ExternalAccounts', 'create', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search2((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalAccounts', 'search2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const externalAccountsUpdate5BodyParam = {
      displayName: 'string',
      marketplace: 'TICKET_CITY',
      integrationProvider: 'NATIVE'
    };
    describe('#update5 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update5(externalAccountsId, externalAccountsUpdate5BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalAccounts', 'update5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById4 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById4(externalAccountsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(10, data.response.accountId);
                assert.equal('string', data.response.displayName);
                assert.equal('TICKET_CITY', data.response.marketplace);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.apiKey);
                assert.equal('NONE', data.response.integrationProvider);
                assert.equal('string', data.response.lastSweep);
                assert.equal(5, data.response.creditCardId);
                assert.equal(9, data.response.creditCardGroupId);
                assert.equal('string', data.response.encryptedPassword);
                assert.equal(7, data.response.customerId);
                assert.equal(2, data.response.vendorId);
                assert.equal(7, data.response.markup);
                assert.equal(7, data.response.fee);
                assert.equal(true, data.response.autoHideSeatNumbers);
                assert.equal(false, data.response.optInStubhubInhandDateLogic);
                assert.equal(false, data.response.optInStubHubTransferDetailsSweep);
                assert.equal(false, data.response.confirmOrders);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalAccounts', 'getById4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let holdsHoldId = 555;
    const holdsInsert3BodyParam = {
      expiryDate: 'string'
    };
    describe('#insert3 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insert3(holdsInsert3BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal(10, data.response.accountId);
                assert.equal(9, data.response.inventoryId);
                assert.equal('string', data.response.user);
                assert.equal(1, data.response.userId);
                assert.equal(3, data.response.removedByUserId);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.expiryDate);
                assert.equal(3, data.response.customerId);
                assert.equal('string', data.response.removedDate);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.externalRef);
                assert.equal(6, data.response.quantity);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.seats);
                assert.equal(8, data.response.cost);
                assert.equal(10, data.response.listPrice);
                assert.equal(1, data.response.eventId);
              } else {
                runCommonAsserts(data, error);
              }
              holdsHoldId = data.response.id;
              saveMockData('Holds', 'insert3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search3 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search3(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Holds', 'search3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const holdsUpdate6BodyParam = {
      expiryDate: 'string'
    };
    describe('#update6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update6(holdsHoldId, holdsUpdate6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Holds', 'update6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById5 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById5(holdsHoldId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal(7, data.response.accountId);
                assert.equal(3, data.response.inventoryId);
                assert.equal('string', data.response.user);
                assert.equal(3, data.response.userId);
                assert.equal(5, data.response.removedByUserId);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.expiryDate);
                assert.equal(3, data.response.customerId);
                assert.equal('string', data.response.removedDate);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.externalRef);
                assert.equal(3, data.response.quantity);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.seats);
                assert.equal(8, data.response.cost);
                assert.equal(2, data.response.listPrice);
                assert.equal(10, data.response.eventId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Holds', 'getById5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryUpdatePurchaseInventoryBodyParam = {
      section: 'string',
      row: 'string'
    };
    describe('#updatePurchaseInventory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePurchaseInventory(inventoryUpdatePurchaseInventoryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'updatePurchaseInventory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryBulkMergeBodyParam = {
      inventoryIds: [
        7
      ]
    };
    describe('#bulkMerge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bulkMerge(inventoryBulkMergeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'bulkMerge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryBulkSwapEventBodyParam = {
      inventoryIds: [
        7
      ],
      eventId: 4
    };
    describe('#bulkSwapEvent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkSwapEvent(inventoryBulkSwapEventBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'bulkSwapEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let inventoryExchangePosId = 'fakedata';
    let inventoryId = 'fakedata';
    const inventoryMergeBodyParam = {
      inventoryIds: [
        6
      ]
    };
    describe('#merge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.merge(inventoryMergeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.inHandDate);
                assert.equal(3, data.response.id);
                assert.equal(1, data.response.accountId);
                assert.equal(9, data.response.eventId);
                assert.equal(10, data.response.quantity);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.secondRow);
                assert.equal(6, data.response.lowSeat);
                assert.equal(1, data.response.highSeat);
                assert.equal(6, data.response.cost);
                assert.equal(4, data.response.faceValue);
                assert.equal(true, Array.isArray(data.response.tickets));
                assert.equal(true, Array.isArray(data.response.ticketIds));
                assert.equal('PAPERLESS', data.response.stockType);
                assert.equal('CUSTOM', data.response.splitType);
                assert.equal('string', data.response.customSplit);
                assert.equal(4, data.response.listPrice);
                assert.equal(7, data.response.expectedValue);
                assert.equal('string', data.response.publicNotes);
                assert.equal(true, Array.isArray(data.response.attributes));
                assert.equal('AVAILABLE', data.response.status);
                assert.equal(5, data.response.inHandDaysBeforeEvent);
                assert.equal('string', data.response.lastPriceUpdate);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('string', data.response.lastDeltaUpdate);
                assert.equal(8, data.response.version);
                assert.equal('string', data.response.tags);
                assert.equal('PIGGYBACK', data.response.seatType);
                assert.equal('object', typeof data.response.eventMapping);
                assert.equal(5, data.response.mappingId);
                assert.equal(8, data.response.exchangePosId);
                assert.equal(false, data.response.broadcast);
                assert.equal(false, data.response.zoneSeating);
                assert.equal(true, data.response.electronicTransfer);
                assert.equal(false, data.response.optOutAutoPrice);
                assert.equal(false, data.response.hideSeatNumbers);
                assert.equal('NONE', data.response.vsrOption);
                assert.equal(1, data.response.replenishmentGroupId);
                assert.equal('string', data.response.replenishmentGroup);
                assert.equal(2, data.response.shownQuantity);
                assert.equal(false, data.response.ticketsMerged);
                assert.equal(true, data.response.ticketsSplit);
                assert.equal('string', data.response.auditNote);
                assert.equal(true, data.response.filesUploaded);
                assert.equal(false, data.response.barCodesEntered);
              } else {
                runCommonAsserts(data, error);
              }
              inventoryExchangePosId = data.response.exchangePosId;
              inventoryId = data.response.id;
              saveMockData('Inventory', 'merge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryMergeAsPiggybackBodyParam = {
      inventoryIds: [
        7
      ]
    };
    describe('#mergeAsPiggyback - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mergeAsPiggyback(inventoryMergeAsPiggybackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.inHandDate);
                assert.equal(5, data.response.id);
                assert.equal(9, data.response.accountId);
                assert.equal(1, data.response.eventId);
                assert.equal(10, data.response.quantity);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.secondRow);
                assert.equal(5, data.response.lowSeat);
                assert.equal(2, data.response.highSeat);
                assert.equal(1, data.response.cost);
                assert.equal(5, data.response.faceValue);
                assert.equal(true, Array.isArray(data.response.tickets));
                assert.equal(true, Array.isArray(data.response.ticketIds));
                assert.equal('PAPERLESS', data.response.stockType);
                assert.equal('NEVERLEAVEONE', data.response.splitType);
                assert.equal('string', data.response.customSplit);
                assert.equal(5, data.response.listPrice);
                assert.equal(1, data.response.expectedValue);
                assert.equal('string', data.response.publicNotes);
                assert.equal(true, Array.isArray(data.response.attributes));
                assert.equal('DEPLETED', data.response.status);
                assert.equal(8, data.response.inHandDaysBeforeEvent);
                assert.equal('string', data.response.lastPriceUpdate);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('string', data.response.lastDeltaUpdate);
                assert.equal(2, data.response.version);
                assert.equal('string', data.response.tags);
                assert.equal('PIGGYBACK', data.response.seatType);
                assert.equal('object', typeof data.response.eventMapping);
                assert.equal(2, data.response.mappingId);
                assert.equal(10, data.response.exchangePosId);
                assert.equal(true, data.response.broadcast);
                assert.equal(true, data.response.zoneSeating);
                assert.equal(true, data.response.electronicTransfer);
                assert.equal(false, data.response.optOutAutoPrice);
                assert.equal(false, data.response.hideSeatNumbers);
                assert.equal('NONE', data.response.vsrOption);
                assert.equal(10, data.response.replenishmentGroupId);
                assert.equal('string', data.response.replenishmentGroup);
                assert.equal(10, data.response.shownQuantity);
                assert.equal(false, data.response.ticketsMerged);
                assert.equal(false, data.response.ticketsSplit);
                assert.equal('string', data.response.auditNote);
                assert.equal(false, data.response.filesUploaded);
                assert.equal(false, data.response.barCodesEntered);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'mergeAsPiggyback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryBulkRemoveTags2BodyParam = [
      5
    ];
    describe('#bulkRemoveTags2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkRemoveTags2(inventoryBulkRemoveTags2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'bulkRemoveTags2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryInventoryId = 555;
    describe('#splitToOriginals - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.splitToOriginals(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'splitToOriginals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryTag2BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#tag2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tag2(inventoryInventoryId, inventoryTag2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'tag2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryDelete4BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete4(inventoryInventoryId, inventoryDelete4BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'delete4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryUpdate8BodyParam = [
      {
        listPrice: 3,
        id: 8,
        broadcast: false
      }
    ];
    describe('#update8 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update8(inventoryUpdate8BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'update8', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryTag = [];
    describe('#search4 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search4(null, null, null, null, null, null, null, inventoryInventoryId, null, inventoryExchangePosId, null, null, inventoryTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'search4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInventoryBarcodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInventoryBarcodes(inventoryExchangePosId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getInventoryBarcodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryBulkUpdateBodyParam = {
      inventoryIds: [
        3
      ]
    };
    describe('#bulkUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkUpdate(inventoryBulkUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'bulkUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryBulkUpdateExpectedValueBodyParam = [
      {
        expectedValue: 5,
        id: 10
      }
    ];
    describe('#bulkUpdateExpectedValue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkUpdateExpectedValue(inventoryBulkUpdateExpectedValueBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'bulkUpdateExpectedValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryStartTime = 'fakedata';
    const inventoryEndTime = 'fakedata';
    describe('#getInventoryDeltas - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInventoryDeltas(inventoryStartTime, inventoryEndTime, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response[0]);
                assert.equal(5, data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getInventoryDeltas', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExchangesPosId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExchangesPosId(inventoryExchangePosId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.exchangePosId);
                assert.equal(5, data.response.inventoryId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getExchangesPosId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#export - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.export(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'export', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchPurchased - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchPurchased(null, null, null, null, null, null, inventoryInventoryId, null, null, inventoryTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'searchPurchased', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchPurchasedV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchPurchasedV2(null, null, null, null, null, null, inventoryInventoryId, null, null, inventoryTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'searchPurchasedV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchSold - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchSold(null, null, null, null, null, inventoryInventoryId, inventoryExchangePosId, null, null, null, null, null, inventoryTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'searchSold', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySyncBodyParam = {
      inventoryIds: [
        3
      ]
    };
    describe('#sync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sync(inventorySyncBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'sync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryUpdate7BodyParam = {
      eventId: 5,
      quantity: 3,
      section: 'string',
      row: 'string',
      cost: 8,
      stockType: 'MOBILE_TRANSFER',
      splitType: 'DEFAULT',
      seatType: 'PIGGYBACK'
    };
    describe('#update7 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update7(inventoryInventoryId, null, inventoryUpdate7BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'update7', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById6(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.inHandDate);
                assert.equal(7, data.response.id);
                assert.equal(1, data.response.accountId);
                assert.equal(7, data.response.eventId);
                assert.equal(9, data.response.quantity);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.secondRow);
                assert.equal(10, data.response.lowSeat);
                assert.equal(8, data.response.highSeat);
                assert.equal(10, data.response.cost);
                assert.equal(10, data.response.faceValue);
                assert.equal(true, Array.isArray(data.response.tickets));
                assert.equal(true, Array.isArray(data.response.ticketIds));
                assert.equal('PAPERLESS', data.response.stockType);
                assert.equal('CUSTOM', data.response.splitType);
                assert.equal('string', data.response.customSplit);
                assert.equal(4, data.response.listPrice);
                assert.equal(10, data.response.expectedValue);
                assert.equal('string', data.response.publicNotes);
                assert.equal(true, Array.isArray(data.response.attributes));
                assert.equal('ON_HOLD', data.response.status);
                assert.equal(9, data.response.inHandDaysBeforeEvent);
                assert.equal('string', data.response.lastPriceUpdate);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('string', data.response.lastDeltaUpdate);
                assert.equal(7, data.response.version);
                assert.equal('string', data.response.tags);
                assert.equal('ALTERNATING', data.response.seatType);
                assert.equal('object', typeof data.response.eventMapping);
                assert.equal(2, data.response.mappingId);
                assert.equal(6, data.response.exchangePosId);
                assert.equal(true, data.response.broadcast);
                assert.equal(true, data.response.zoneSeating);
                assert.equal(true, data.response.electronicTransfer);
                assert.equal(false, data.response.optOutAutoPrice);
                assert.equal(true, data.response.hideSeatNumbers);
                assert.equal('NONE', data.response.vsrOption);
                assert.equal(1, data.response.replenishmentGroupId);
                assert.equal('string', data.response.replenishmentGroup);
                assert.equal(9, data.response.shownQuantity);
                assert.equal(false, data.response.ticketsMerged);
                assert.equal(true, data.response.ticketsSplit);
                assert.equal('string', data.response.auditNote);
                assert.equal(false, data.response.filesUploaded);
                assert.equal(true, data.response.barCodesEntered);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getById6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHoldsById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHoldsById(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal(10, data.response.accountId);
                assert.equal(5, data.response.inventoryId);
                assert.equal('string', data.response.user);
                assert.equal(1, data.response.userId);
                assert.equal(3, data.response.removedByUserId);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.expiryDate);
                assert.equal(6, data.response.customerId);
                assert.equal('string', data.response.removedDate);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.externalRef);
                assert.equal(8, data.response.quantity);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.seats);
                assert.equal(9, data.response.cost);
                assert.equal(3, data.response.listPrice);
                assert.equal(3, data.response.eventId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getHoldsById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#priceHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.priceHistory(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'priceHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySplitBodyParam = {};
    describe('#split - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.split(inventoryInventoryId, inventorySplitBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'split', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#split1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.split1(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'split1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryUpdateTicketsBodyParam = [
      {
        ticketId: 5,
        barCode: 'string',
        fileName: 'string',
        base64FileBytes: 'string'
      }
    ];
    describe('#updateTickets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTickets(inventoryInventoryId, inventoryUpdateTicketsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'updateTickets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTicketsById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTicketsById(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getTicketsById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let invoicesInvoiceId = 555;
    let invoicesTransactionId = 555;
    let invoicesLineId = 555;
    const invoicesInsertInvoiceBodyParam = {
      lines: [
        {}
      ],
      salesTerm: 'NET30',
      taxAmount: 6,
      shippingAmount: 3,
      otherAmount: 2,
      paymentStatus: 'PARTIAL',
      fulfillmentStatus: 'PENDING'
    };
    describe('#insertInvoice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertInvoice(null, invoicesInsertInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(5, data.response.customerId);
                assert.equal(7, data.response.accountId);
                assert.equal(5, data.response.internalId);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal('NET0', data.response.salesTerm);
                assert.equal('CASH', data.response.paymentMethod);
                assert.equal('string', data.response.paymentRef);
                assert.equal('EMAIL', data.response.deliveryMethod);
                assert.equal(10, data.response.shippingAddressId);
                assert.equal(2, data.response.billingAddressId);
                assert.equal(7, data.response.taxAmount);
                assert.equal(10, data.response.shippingAmount);
                assert.equal(7, data.response.otherAmount);
                assert.equal('string', data.response.internalNotes);
                assert.equal('string', data.response.publicNotes);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.dueDate);
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('PAID', data.response.paymentStatus);
                assert.equal('PENDING', data.response.fulfillmentStatus);
                assert.equal('string', data.response.externalRef);
                assert.equal('string', data.response.airbill);
                assert.equal('MANAGER_REVIEW', data.response.status);
                assert.equal('AUD', data.response.currencyCode);
                assert.equal(true, Array.isArray(data.response.payments));
                assert.equal('object', typeof data.response.invoiceDeliveryLink);
                assert.equal(true, Array.isArray(data.response.notes));
                assert.equal('string', data.response.fulfillmentDate);
                assert.equal(6, data.response.fulfillmentByUserId);
                assert.equal(9, data.response.outstandingBalance);
                assert.equal(false, data.response.barcodesEntered);
                assert.equal(true, data.response.filesUploaded);
              } else {
                runCommonAsserts(data, error);
              }
              invoicesInvoiceId = data.response.id;
              invoicesTransactionId = data.response.id;
              invoicesLineId = data.response.id;
              saveMockData('Invoices', 'insertInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesBulkRemoveTags3BodyParam = [
      1
    ];
    describe('#bulkRemoveTags3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkRemoveTags3(invoicesBulkRemoveTags3BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'bulkRemoveTags3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesInsertInvoiceLineBodyParam = {
      accountId: 3,
      amount: 9,
      lineItemType: 'INVENTORY'
    };
    describe('#insertInvoiceLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertInvoiceLine(invoicesInvoiceId, invoicesInsertInvoiceLineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal(9, data.response.accountId);
                assert.equal(2, data.response.targetId);
                assert.equal(1, data.response.quantity);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.amount);
                assert.equal('PURCHASE', data.response.lineType);
                assert.equal('INVENTORY', data.response.lineItemType);
                assert.equal(true, Array.isArray(data.response.itemIds));
                assert.equal('object', typeof data.response.inventory);
                assert.equal(false, data.response.cancelled);
                assert.equal(true, data.response.delete);
                assert.equal(false, data.response.cancel);
                assert.equal(8, data.response.fillLineId);
                assert.equal(true, Array.isArray(data.response.inventoryIds));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'insertInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesProcessPaymentBodyParam = {
      amount: 6,
      comments: 'string',
      card: {
        token: 'string',
        billingAddress: {
          firstName: 'string',
          lastName: 'string',
          email: 'string',
          address1: 'string',
          address2: 'string',
          city: 'string',
          state: 'FL',
          postalCode: 'string',
          phone: 'string',
          country: 'string',
          persistInVault: true
        },
        cardholderName: 'string',
        defaultCard: false
      },
      creditCard: {
        token: 'string',
        billingAddress: {
          firstName: 'string',
          lastName: 'string',
          email: 'string',
          address1: 'string',
          address2: 'string',
          city: 'string',
          state: 'MD',
          postalCode: 'string',
          phone: 'string',
          country: 'string',
          persistInVault: false
        },
        cardholderName: 'string',
        defaultCard: false
      }
    };
    describe('#processPayment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.processPayment(invoicesInvoiceId, invoicesProcessPaymentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'processPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesTag3BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#tag3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tag3(invoicesInvoiceId, invoicesTag3BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'tag3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesDelete5BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete5 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete5(invoicesInvoiceId, invoicesDelete5BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'delete5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesProcessRefundBodyParam = {
      amount: 2,
      comments: 'string'
    };
    describe('#processRefund - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.processRefund(invoicesInvoiceId, invoicesTransactionId, invoicesProcessRefundBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'processRefund', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesExternalRef = 'fake data';
    const invoicesTag = [];
    describe('#search5 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search5(null, null, null, null, invoicesTag, null, null, null, null, null, null, null, null, null, null, invoicesExternalRef, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'search5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesUpdateCurrencyBodyParam = {
      invoiceIds: [
        8
      ],
      invoiceLineIds: [
        2
      ],
      currencyCode: 'XBT'
    };
    describe('#updateCurrency - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCurrency(invoicesUpdateCurrencyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'updateCurrency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesUpdate9BodyParam = {
      invoiceIds: [
        4
      ]
    };
    describe('#update9 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update9(invoicesUpdate9BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'update9', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceTicketsByExternalRefV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInvoiceTicketsByExternalRefV2(invoicesExternalRef, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceTicketsByExternalRefV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceTicketsByExternalRef - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInvoiceTicketsByExternalRef(invoicesExternalRef, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceTicketsByExternalRef', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesUpdateInvoiceBodyParam = {
      lines: [
        {}
      ],
      salesTerm: 'NET0',
      taxAmount: 9,
      shippingAmount: 3,
      otherAmount: 8,
      paymentStatus: 'VOID',
      fulfillmentStatus: 'COMPLETE'
    };
    describe('#updateInvoice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateInvoice(invoicesInvoiceId, invoicesUpdateInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'updateInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInvoiceById(invoicesInvoiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal(8, data.response.customerId);
                assert.equal(3, data.response.accountId);
                assert.equal(1, data.response.internalId);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal('NET0', data.response.salesTerm);
                assert.equal('EVOPAY', data.response.paymentMethod);
                assert.equal('string', data.response.paymentRef);
                assert.equal('EMAIL', data.response.deliveryMethod);
                assert.equal(7, data.response.shippingAddressId);
                assert.equal(5, data.response.billingAddressId);
                assert.equal(6, data.response.taxAmount);
                assert.equal(6, data.response.shippingAmount);
                assert.equal(8, data.response.otherAmount);
                assert.equal('string', data.response.internalNotes);
                assert.equal('string', data.response.publicNotes);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.dueDate);
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('PAID', data.response.paymentStatus);
                assert.equal('COMPLETE', data.response.fulfillmentStatus);
                assert.equal('string', data.response.externalRef);
                assert.equal('string', data.response.airbill);
                assert.equal('MANAGER_REVIEW', data.response.status);
                assert.equal('AUD', data.response.currencyCode);
                assert.equal(true, Array.isArray(data.response.payments));
                assert.equal('object', typeof data.response.invoiceDeliveryLink);
                assert.equal(true, Array.isArray(data.response.notes));
                assert.equal('string', data.response.fulfillmentDate);
                assert.equal(4, data.response.fulfillmentByUserId);
                assert.equal(2, data.response.outstandingBalance);
                assert.equal(true, data.response.barcodesEntered);
                assert.equal(false, data.response.filesUploaded);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAssets(invoicesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getAssets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceLines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInvoiceLines(invoicesInvoiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesUpdateInvoiceLineBodyParam = {
      accountId: 8,
      amount: 9,
      lineItemType: 'GENERIC'
    };
    describe('#updateInvoiceLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateInvoiceLine(invoicesInvoiceId, invoicesLineId, invoicesUpdateInvoiceLineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'updateInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInvoiceLine(invoicesInvoiceId, invoicesLineId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal(6, data.response.accountId);
                assert.equal(2, data.response.targetId);
                assert.equal(9, data.response.quantity);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.amount);
                assert.equal('PURCHASE', data.response.lineType);
                assert.equal('INVENTORY', data.response.lineItemType);
                assert.equal(true, Array.isArray(data.response.itemIds));
                assert.equal('object', typeof data.response.inventory);
                assert.equal(true, data.response.cancelled);
                assert.equal(false, data.response.delete);
                assert.equal(false, data.response.cancel);
                assert.equal(1, data.response.fillLineId);
                assert.equal(true, Array.isArray(data.response.inventoryIds));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceLineTickets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInvoiceLineTickets(invoicesInvoiceId, invoicesLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceLineTickets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#print - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.print(invoicesInvoiceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'print', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#printCustomAuthForm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.printCustomAuthForm(invoicesInvoiceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'printCustomAuthForm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesEmailAddress = [];
    describe('#send - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.send(invoicesInvoiceId, invoicesEmailAddress, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'send', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransactionHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransactionHistory(invoicesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getTransactionHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linesLineId = 555;
    describe('#getLineById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLineById(linesLineId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(6, data.response.accountId);
                assert.equal(9, data.response.targetId);
                assert.equal(6, data.response.quantity);
                assert.equal('string', data.response.description);
                assert.equal(6, data.response.amount);
                assert.equal('INVOICE', data.response.lineType);
                assert.equal('INVENTORY', data.response.lineItemType);
                assert.equal(true, Array.isArray(data.response.itemIds));
                assert.equal('object', typeof data.response.inventory);
                assert.equal(false, data.response.cancelled);
                assert.equal(false, data.response.delete);
                assert.equal(true, data.response.cancel);
                assert.equal(7, data.response.fillLineId);
                assert.equal(true, Array.isArray(data.response.inventoryIds));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Lines', 'getLineById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mappingBulkDeleteBodyParam = [
      10
    ];
    describe('#bulkDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkDelete(mappingBulkDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mapping', 'bulkDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPendingByAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPendingByAccount(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mapping', 'getPendingByAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mappingMappingId = 555;
    describe('#findPendingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findPendingById(mappingMappingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.eventName);
                assert.equal('string', data.response.venueName);
                assert.equal('string', data.response.eventDate);
                assert.equal(2, data.response.id);
                assert.equal(6, data.response.accountId);
                assert.equal(4, data.response.purchaseLineId);
                assert.equal('string', data.response.createdDate);
                assert.equal(3, data.response.responseStatus);
                assert.equal(4, data.response.sendCount);
                assert.equal(6, data.response.mappedByUserId);
                assert.equal('string', data.response.mappedDate);
                assert.equal(true, data.response.valid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mapping', 'findPendingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resendMappingRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resendMappingRequest(mappingMappingId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mapping', 'resendMappingRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let purchasesPurchaseId = 555;
    let purchasesLineId = 555;
    const purchasesInsertPurchaseBodyParam = {
      lines: [
        {}
      ],
      purchaseTerm: 'NET60',
      paymentMethod: 'CHECK',
      deliveryMethod: 'USPS',
      taxAmount: 5,
      shippingAmount: 1,
      otherAmount: 1,
      dueDate: 'string',
      paymentStatus: 'PARTIAL',
      currencyCode: 'BRL'
    };
    describe('#insertPurchase - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPurchase(purchasesInsertPurchaseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(4, data.response.vendorId);
                assert.equal(10, data.response.accountId);
                assert.equal(4, data.response.internalId);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal('NET10', data.response.purchaseTerm);
                assert.equal('MULTIPLE', data.response.paymentMethod);
                assert.equal('string', data.response.paymentRef);
                assert.equal('FLASHSEATS', data.response.deliveryMethod);
                assert.equal(5, data.response.shippingAddressId);
                assert.equal(4, data.response.billingAddressId);
                assert.equal(9, data.response.taxAmount);
                assert.equal(6, data.response.shippingAmount);
                assert.equal(6, data.response.otherAmount);
                assert.equal('string', data.response.internalNotes);
                assert.equal('string', data.response.publicNotes);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.dueDate);
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('string', data.response.externalRef);
                assert.equal('VOID', data.response.paymentStatus);
                assert.equal(10, data.response.creditCardId);
                assert.equal(5, data.response.creditCardGroupId);
                assert.equal('EUR', data.response.currencyCode);
                assert.equal(true, Array.isArray(data.response.payments));
                assert.equal(true, Array.isArray(data.response.notes));
                assert.equal(false, data.response.received);
                assert.equal('object', typeof data.response.consignment);
                assert.equal(false, data.response.cooperative);
                assert.equal('CONTACT_NEEDED', data.response.status);
                assert.equal('string', data.response.airbill);
                assert.equal(7, data.response.outstandingBalance);
                assert.equal(false, data.response.autoPo);
              } else {
                runCommonAsserts(data, error);
              }
              purchasesPurchaseId = data.response.id;
              purchasesLineId = data.response.id;
              saveMockData('Purchases', 'insertPurchase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesBulkRemoveTags4BodyParam = [
      5
    ];
    describe('#bulkRemoveTags4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkRemoveTags4(purchasesBulkRemoveTags4BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'bulkRemoveTags4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesMultiWithPurchaseIdBodyParam = [
      {
        inHandDate: 'string',
        id: 7,
        accountId: 6,
        eventId: 1,
        quantity: 8,
        notes: 'string',
        section: 'string',
        row: 'string',
        secondRow: 'string',
        lowSeat: 10,
        highSeat: 2,
        cost: 8,
        faceValue: 6,
        tickets: [
          {
            id: 5,
            seatNumber: 1,
            fileName: 'string',
            barCode: 'string',
            inventoryId: 5,
            invoiceLineId: 3,
            purchaseLineId: 1,
            section: 'string',
            row: 'string',
            notes: 'string',
            cost: 7,
            faceValue: 1,
            sellPrice: 6,
            stockType: 'ELECTRONIC',
            eventId: 6,
            accountId: 1,
            status: 'CANCELLED',
            base64FileBytes: 'string',
            disclosures: [
              'string',
              'string'
            ],
            attributes: [
              'string',
              'string'
            ],
            createdDate: 'string',
            createdBy: 'string',
            lastUpdate: 'string',
            lastUpdateBy: 'string',
            dateCancelled: 'string',
            cancelledByUserId: 3,
            auditNote: 'string'
          },
          {
            id: 4,
            seatNumber: 5,
            fileName: 'string',
            barCode: 'string',
            inventoryId: 6,
            invoiceLineId: 8,
            purchaseLineId: 6,
            section: 'string',
            row: 'string',
            notes: 'string',
            cost: 3,
            faceValue: 1,
            sellPrice: 10,
            stockType: 'ELECTRONIC',
            eventId: 10,
            accountId: 10,
            status: 'AVAILABLE',
            base64FileBytes: 'string',
            disclosures: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            attributes: [
              'string',
              'string',
              'string',
              'string'
            ],
            createdDate: 'string',
            createdBy: 'string',
            lastUpdate: 'string',
            lastUpdateBy: 'string',
            dateCancelled: 'string',
            cancelledByUserId: 3,
            auditNote: 'string'
          },
          {
            id: 8,
            seatNumber: 8,
            fileName: 'string',
            barCode: 'string',
            inventoryId: 6,
            invoiceLineId: 9,
            purchaseLineId: 5,
            section: 'string',
            row: 'string',
            notes: 'string',
            cost: 7,
            faceValue: 5,
            sellPrice: 7,
            stockType: 'HARD',
            eventId: 2,
            accountId: 7,
            status: 'AVAILABLE',
            base64FileBytes: 'string',
            disclosures: [
              'string'
            ],
            attributes: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            createdDate: 'string',
            createdBy: 'string',
            lastUpdate: 'string',
            lastUpdateBy: 'string',
            dateCancelled: 'string',
            cancelledByUserId: 3,
            auditNote: 'string'
          },
          {
            id: 9,
            seatNumber: 1,
            fileName: 'string',
            barCode: 'string',
            inventoryId: 4,
            invoiceLineId: 7,
            purchaseLineId: 1,
            section: 'string',
            row: 'string',
            notes: 'string',
            cost: 4,
            faceValue: 4,
            sellPrice: 5,
            stockType: 'PAPERLESS_CARD',
            eventId: 4,
            accountId: 4,
            status: 'SOLD',
            base64FileBytes: 'string',
            disclosures: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            attributes: [
              'string'
            ],
            createdDate: 'string',
            createdBy: 'string',
            lastUpdate: 'string',
            lastUpdateBy: 'string',
            dateCancelled: 'string',
            cancelledByUserId: 2,
            auditNote: 'string'
          },
          {
            id: 3,
            seatNumber: 7,
            fileName: 'string',
            barCode: 'string',
            inventoryId: 1,
            invoiceLineId: 4,
            purchaseLineId: 2,
            section: 'string',
            row: 'string',
            notes: 'string',
            cost: 9,
            faceValue: 5,
            sellPrice: 8,
            stockType: 'PAPERLESS',
            eventId: 10,
            accountId: 8,
            status: 'AVAILABLE',
            base64FileBytes: 'string',
            disclosures: [
              'string',
              'string',
              'string'
            ],
            attributes: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            createdDate: 'string',
            createdBy: 'string',
            lastUpdate: 'string',
            lastUpdateBy: 'string',
            dateCancelled: 'string',
            cancelledByUserId: 4,
            auditNote: 'string'
          },
          {
            id: 7,
            seatNumber: 3,
            fileName: 'string',
            barCode: 'string',
            inventoryId: 5,
            invoiceLineId: 4,
            purchaseLineId: 1,
            section: 'string',
            row: 'string',
            notes: 'string',
            cost: 6,
            faceValue: 1,
            sellPrice: 1,
            stockType: 'ELECTRONIC',
            eventId: 1,
            accountId: 4,
            status: 'CANCELLED',
            base64FileBytes: 'string',
            disclosures: [
              'string'
            ],
            attributes: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            createdDate: 'string',
            createdBy: 'string',
            lastUpdate: 'string',
            lastUpdateBy: 'string',
            dateCancelled: 'string',
            cancelledByUserId: 5,
            auditNote: 'string'
          }
        ],
        ticketIds: [
          1,
          4,
          1,
          9,
          4,
          2,
          9,
          4,
          1,
          6
        ],
        stockType: 'FLASH',
        splitType: 'ANY',
        customSplit: 'string',
        listPrice: 4,
        expectedValue: 7,
        publicNotes: 'string',
        attributes: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        status: 'AVAILABLE',
        inHandDaysBeforeEvent: 5,
        lastPriceUpdate: 'string',
        createdDate: 'string',
        createdBy: 'string',
        lastUpdate: 'string',
        lastUpdateBy: 'string',
        lastDeltaUpdate: 'string',
        version: 2,
        tags: 'string',
        seatType: 'PIGGYBACK',
        eventMapping: {
          eventName: 'string',
          venueName: 'string',
          eventDate: 'string',
          valid: false
        },
        mappingId: 5,
        exchangePosId: 3,
        broadcast: false,
        zoneSeating: false,
        electronicTransfer: false,
        optOutAutoPrice: false,
        hideSeatNumbers: true,
        vsrOption: 'ALL',
        replenishmentGroupId: 7,
        replenishmentGroup: 'string',
        shownQuantity: 6,
        ticketsMerged: false,
        ticketsSplit: false,
        auditNote: 'string',
        filesUploaded: true,
        barCodesEntered: false
      }
    ];
    describe('#multiWithPurchaseId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.multiWithPurchaseId(purchasesPurchaseId, purchasesMultiWithPurchaseIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'multiWithPurchaseId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesInsertPurchaseLineBodyParam = {
      accountId: 7,
      amount: 9,
      lineItemType: 'INVENTORY'
    };
    describe('#insertPurchaseLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPurchaseLine(purchasesPurchaseId, purchasesInsertPurchaseLineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal(3, data.response.accountId);
                assert.equal(4, data.response.targetId);
                assert.equal(4, data.response.quantity);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.amount);
                assert.equal('INVOICE', data.response.lineType);
                assert.equal('INVENTORY', data.response.lineItemType);
                assert.equal(true, Array.isArray(data.response.itemIds));
                assert.equal('object', typeof data.response.inventory);
                assert.equal(true, data.response.cancelled);
                assert.equal(false, data.response.delete);
                assert.equal(false, data.response.cancel);
                assert.equal(3, data.response.fillLineId);
                assert.equal(true, Array.isArray(data.response.inventoryIds));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'insertPurchaseLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesReplacePurchaseLineBodyParam = {
      section: 'string',
      row: 'string',
      lowSeat: 1,
      force: true,
      seatType: 'CONSECUTIVE'
    };
    describe('#replacePurchaseLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replacePurchaseLine(purchasesPurchaseId, purchasesLineId, purchasesReplacePurchaseLineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal(10, data.response.accountId);
                assert.equal(4, data.response.targetId);
                assert.equal(5, data.response.quantity);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.amount);
                assert.equal('INVOICE', data.response.lineType);
                assert.equal('INVENTORY', data.response.lineItemType);
                assert.equal(true, Array.isArray(data.response.itemIds));
                assert.equal('object', typeof data.response.inventory);
                assert.equal(true, data.response.cancelled);
                assert.equal(false, data.response.delete);
                assert.equal(false, data.response.cancel);
                assert.equal(4, data.response.fillLineId);
                assert.equal(true, Array.isArray(data.response.inventoryIds));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'replacePurchaseLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesUpdatePurchaseInventory1BodyParam = {
      section: 'string',
      row: 'string'
    };
    describe('#updatePurchaseInventory1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePurchaseInventory1(purchasesPurchaseId, purchasesLineId, purchasesUpdatePurchaseInventory1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'updatePurchaseInventory1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesAddPurchaseNoteBodyParam = {
      text: 'string'
    };
    describe('#addPurchaseNote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPurchaseNote(purchasesPurchaseId, purchasesAddPurchaseNoteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal(2, data.response.userId);
                assert.equal(1, data.response.accountId);
                assert.equal(1, data.response.purchaseId);
                assert.equal('string', data.response.date);
                assert.equal('string', data.response.text);
                assert.equal('string', data.response.author);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'addPurchaseNote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesTag4BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#tag4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tag4(purchasesPurchaseId, purchasesTag4BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'tag4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesDelete7BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete7 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete7(purchasesPurchaseId, purchasesDelete7BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'delete7', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesTag = [];
    describe('#search6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search6(null, null, null, null, null, null, purchasesTag, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'search6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesUpdateCurrency1BodyParam = {
      purchaseIds: [
        8
      ],
      purchaseLineIds: [
        7
      ],
      currencyCode: 'XBT'
    };
    describe('#updateCurrency1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCurrency1(purchasesUpdateCurrency1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'updateCurrency1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAutoPurchases - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAutoPurchases(null, null, null, null, purchasesPurchaseId, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'searchAutoPurchases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesUpdate10BodyParam = {
      purchaseIds: [
        6
      ]
    };
    describe('#update10 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update10(purchasesUpdate10BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'update10', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesUpdatePurchaseBodyParam = {
      lines: [
        {}
      ],
      purchaseTerm: 'NET25',
      paymentMethod: 'ACH',
      deliveryMethod: 'MEETANDGREET',
      taxAmount: 9,
      shippingAmount: 4,
      otherAmount: 8,
      dueDate: 'string',
      paymentStatus: 'UNPAID',
      currencyCode: 'GBP'
    };
    describe('#updatePurchase - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePurchase(purchasesPurchaseId, purchasesUpdatePurchaseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'updatePurchase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPurchaseById(purchasesPurchaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(4, data.response.vendorId);
                assert.equal(4, data.response.accountId);
                assert.equal(3, data.response.internalId);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal('NET15', data.response.purchaseTerm);
                assert.equal('TRADE', data.response.paymentMethod);
                assert.equal('string', data.response.paymentRef);
                assert.equal('FLASHSEATS', data.response.deliveryMethod);
                assert.equal(9, data.response.shippingAddressId);
                assert.equal(1, data.response.billingAddressId);
                assert.equal(8, data.response.taxAmount);
                assert.equal(9, data.response.shippingAmount);
                assert.equal(3, data.response.otherAmount);
                assert.equal('string', data.response.internalNotes);
                assert.equal('string', data.response.publicNotes);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.dueDate);
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('string', data.response.externalRef);
                assert.equal('PAID', data.response.paymentStatus);
                assert.equal(10, data.response.creditCardId);
                assert.equal(7, data.response.creditCardGroupId);
                assert.equal('JPY', data.response.currencyCode);
                assert.equal(true, Array.isArray(data.response.payments));
                assert.equal(true, Array.isArray(data.response.notes));
                assert.equal(true, data.response.received);
                assert.equal('object', typeof data.response.consignment);
                assert.equal(false, data.response.cooperative);
                assert.equal('INVESTIGATION_CLOSED', data.response.status);
                assert.equal('string', data.response.airbill);
                assert.equal(2, data.response.outstandingBalance);
                assert.equal(false, data.response.autoPo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'getPurchaseById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAirbill - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAirbill(purchasesPurchaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal(2, data.response.userId);
                assert.equal(1, data.response.accountId);
                assert.equal(6, data.response.purchaseId);
                assert.equal('string', data.response.date);
                assert.equal('string', data.response.text);
                assert.equal('string', data.response.author);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'getAirbill', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search7 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search7(purchasesPurchaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'search7', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseLines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPurchaseLines(purchasesPurchaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'getPurchaseLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesUpdatePurchaseLineBodyParam = {
      accountId: 4,
      amount: 4,
      lineItemType: 'INVENTORY'
    };
    describe('#updatePurchaseLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePurchaseLine(purchasesPurchaseId, purchasesLineId, purchasesUpdatePurchaseLineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'updatePurchaseLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseLine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPurchaseLine(purchasesPurchaseId, purchasesLineId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal(8, data.response.accountId);
                assert.equal(4, data.response.targetId);
                assert.equal(7, data.response.quantity);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.amount);
                assert.equal('PURCHASE', data.response.lineType);
                assert.equal('INVENTORY', data.response.lineItemType);
                assert.equal(true, Array.isArray(data.response.itemIds));
                assert.equal('object', typeof data.response.inventory);
                assert.equal(true, data.response.cancelled);
                assert.equal(true, data.response.delete);
                assert.equal(true, data.response.cancel);
                assert.equal(2, data.response.fillLineId);
                assert.equal(true, Array.isArray(data.response.inventoryIds));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'getPurchaseLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelPurchaseLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelPurchaseLine(purchasesPurchaseId, purchasesLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'cancelPurchaseLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseLineTickets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPurchaseLineTickets(purchasesPurchaseId, purchasesLineId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'getPurchaseLineTickets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#print1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.print1(purchasesPurchaseId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'print1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasesEmailAddress = [];
    describe('#send1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.send1(purchasesPurchaseId, purchasesEmailAddress, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'send1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickSearchKey = 'fakedata';
    describe('#search8 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search8(quickSearchKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.entityId);
                assert.equal('INVENTORY', data.response.entityType);
                assert.equal('string', data.response.matchedField);
                assert.equal('string', data.response.externalRef);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickSearch', 'search8', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryReportSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.queryReportSnapshots(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'queryReportSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportSnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportSnapshot(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal(3, data.response.accountId);
                assert.equal(1, data.response.scheduleId);
                assert.equal(9, data.response.reportId);
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal('string', data.response.assetName);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.scheduledDate);
                assert.equal(10, data.response.number);
                assert.equal('string', data.response.notes);
                assert.equal(true, Array.isArray(data.response.notificationEmails));
                assert.equal('object', typeof data.response.report);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getReportSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsDelete8BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete8 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete8(tagsDelete8BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'delete8', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#list - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.list((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'list', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsBulkBodyParam = {
      entityIds: [
        2
      ],
      entityType: 'EVENT'
    };
    describe('#bulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bulk(tagsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'bulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tagsSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tagsSummary(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'tagsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search9 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search9(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal(1, data.response.seatNumber);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.barCode);
                assert.equal(10, data.response.inventoryId);
                assert.equal(6, data.response.invoiceLineId);
                assert.equal(4, data.response.purchaseLineId);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.row);
                assert.equal('string', data.response.notes);
                assert.equal(9, data.response.cost);
                assert.equal(6, data.response.faceValue);
                assert.equal(2, data.response.sellPrice);
                assert.equal('FLASH', data.response.stockType);
                assert.equal(2, data.response.eventId);
                assert.equal(5, data.response.accountId);
                assert.equal('SOLD', data.response.status);
                assert.equal('string', data.response.base64FileBytes);
                assert.equal(true, Array.isArray(data.response.disclosures));
                assert.equal(true, Array.isArray(data.response.attributes));
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastUpdateBy);
                assert.equal('string', data.response.dateCancelled);
                assert.equal(2, data.response.cancelledByUserId);
                assert.equal('string', data.response.auditNote);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tickets', 'search9', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listUsers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'listUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMe((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getMe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsInsert4BodyParam = {
      email: 'string',
      address: {},
      displayName: 'string',
      type: 'BROKER',
      paymentMethod: 'TRADE',
      purchaseTerm: 'NET60',
      defaultDeliveryMethod: 'UPS'
    };
    describe('#insert4 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insert4(vendorsInsert4BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'insert4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsBulkRemoveTags5BodyParam = [
      3
    ];
    describe('#bulkRemoveTags5 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkRemoveTags5(vendorsBulkRemoveTags5BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'bulkRemoveTags5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsVendorId = 555;
    const vendorsTag5BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#tag5 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tag5(vendorsVendorId, vendorsTag5BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'tag5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsDelete9BodyParam = [
      {
        tag: 'string'
      }
    ];
    describe('#delete9 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete9(vendorsVendorId, vendorsDelete9BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'delete9', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsTag = [];
    describe('#search10 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search10(null, null, null, null, null, vendorsTag, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'search10', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsUpdate11BodyParam = {
      vendorIds: [
        5
      ],
      paymentMethod: 'VENMO'
    };
    describe('#update11 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update11(vendorsUpdate11BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'update11', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsUpdateDefaultVendorBodyParam = {};
    describe('#updateDefaultVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDefaultVendor(vendorsUpdateDefaultVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'updateDefaultVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefault - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefault((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'getDefault', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsUpdate12BodyParam = {
      email: 'string',
      address: {},
      displayName: 'string',
      type: 'PRIMARY',
      paymentMethod: 'CREDITCARD',
      purchaseTerm: 'NET15',
      defaultDeliveryMethod: 'FLASHSEATS'
    };
    describe('#update12 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update12(vendorsVendorId, vendorsUpdate12BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'update12', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getById7 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById7(vendorsVendorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal(5, data.response.accountId);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.email2);
                assert.equal(false, data.response.useEmail2);
                assert.equal('string', data.response.company);
                assert.equal('object', typeof data.response.address);
                assert.equal('string', data.response.notes);
                assert.equal('string', data.response.displayName);
                assert.equal('INDY', data.response.type);
                assert.equal('VENMO', data.response.paymentMethod);
                assert.equal('NET0', data.response.purchaseTerm);
                assert.equal(true, data.response.deleted);
                assert.equal('string', data.response.tags);
                assert.equal('MOBILE_DELIVERY', data.response.defaultDeliveryMethod);
                assert.equal(false, data.response.accountDefault);
                assert.equal(2, data.response.clientId);
                assert.equal(6, data.response.brokerId);
                assert.equal(8, data.response.vsrAccountId);
                assert.equal(true, Array.isArray(data.response.outlawedPaymentMethods));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'getById7', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search11 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search11(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Venues', 'search11', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const venuesVenueId = 555;
    describe('#getById8 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getById8(venuesVenueId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.address);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.postalCode);
                assert.equal('string', data.response.phone);
                assert.equal('object', typeof data.response.timeZone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Venues', 'getById8', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webhooksId = 'fakedata';
    const webhooksCreate1BodyParam = {
      topic: 'string',
      url: 'string'
    };
    describe('#create1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.create1(webhooksCreate1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.headers);
                assert.equal('string', data.response.secret);
                assert.equal(false, data.response.verifySsl);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              webhooksId = data.response.id;
              saveMockData('Webhooks', 'create1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#query1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.query1((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'query1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksUpdate13BodyParam = {
      topic: 'string',
      url: 'string'
    };
    describe('#update13 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update13(webhooksId, webhooksUpdate13BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'update13', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#find - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.find(webhooksId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.headers);
                assert.equal('string', data.response.secret);
                assert.equal(false, data.response.verifySsl);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'find', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteById(addressesAddressId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addresses', 'deleteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acknowledge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.acknowledge(alertsAlertId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'acknowledge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroup(creditCardsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'deleteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete(creditCardsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CreditCards', 'delete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeCard - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeCard(customersCustomerId, customersCardId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'removeCard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTag(customersCustomerId, customersTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customers', 'removeTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTag1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTag1(eventsEventId, eventsTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'removeTag1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete3(externalAccountsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExternalAccounts', 'delete3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteById1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteById1(holdsHoldId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Holds', 'deleteById1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#legacyBulkRemoveTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.legacyBulkRemoveTags(inventoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'legacyBulkRemoveTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHoldsById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteHoldsById(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'deleteHoldsById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTag2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTag2(inventoryInventoryId, inventoryTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'removeTag2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancel(inventoryInventoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'cancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInvoiceLine(invoicesInvoiceId, invoicesLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'deleteInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTag3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTag3(invoicesInvoiceId, invoicesTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'removeTag3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete6 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete6(mappingMappingId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mapping', 'delete6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTag4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTag4(purchasesPurchaseId, purchasesTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchases', 'removeTag4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTag5 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTag5(vendorsVendorId, vendorsTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'removeTag5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete10 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete10(webhooksId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-skybox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'delete10', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
