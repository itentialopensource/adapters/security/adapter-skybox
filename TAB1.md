# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Skybox System. The API that was used to build the adapter for Skybox is usually available in the report directory of this adapter. The adapter utilizes the Skybox API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Skybox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Skybox. With this adapter you have the ability to perform operations such as:

- Account
- Customer
- Alerts
- Events

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
