
## 0.8.4 [10-15-2024]

* Changes made at 2024.10.14_21:16PM

See merge request itentialopensource/adapters/adapter-skybox!19

---

## 0.8.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-skybox!17

---

## 0.8.2 [08-15-2024]

* Changes made at 2024.08.14_19:31PM

See merge request itentialopensource/adapters/adapter-skybox!16

---

## 0.8.1 [08-07-2024]

* Changes made at 2024.08.06_21:34PM

See merge request itentialopensource/adapters/adapter-skybox!15

---

## 0.8.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-skybox!14

---

## 0.7.4 [03-27-2024]

* Changes made at 2024.03.27_13:51PM

See merge request itentialopensource/adapters/security/adapter-skybox!13

---

## 0.7.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/security/adapter-skybox!12

---

## 0.7.2 [03-11-2024]

* Changes made at 2024.03.11_16:20PM

See merge request itentialopensource/adapters/security/adapter-skybox!11

---

## 0.7.1 [02-27-2024]

* Changes made at 2024.02.27_11:55AM

See merge request itentialopensource/adapters/security/adapter-skybox!10

---

## 0.7.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-skybox!9

---
