# Skybox

Vendor: Skybox Security
Homepage: https://www.skyboxsecurity.com/

Product: Skybox Security
Product Page: https://www.skyboxsecurity.com/

## Introduction
We classify Skybox into the Security/SASE domain as Skybox provide a Security solution.

"Continuously assess, prioritize, and remediate the cyber threats that matter most across your hybrid attack surface." 

## Why Integrate
The Skybox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Skybox. With this adapter you have the ability to perform operations such as:

- Account
- Customer
- Alerts
- Events

## Additional Product Documentation
The [Skybox API](https://skybox.vividseats.com/api-docs/)
