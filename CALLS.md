## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Skybox. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Skybox.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Skybox. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getNotifications(callback)</td>
    <td style="padding:15px">Get account notifications</td>
    <td style="padding:15px">{base_path}/{version}/account/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">Gets an account</td>
    <td style="padding:15px">{base_path}/{version}/account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById(addressId, callback)</td>
    <td style="padding:15px">Retrieves an address by the address id</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(addressId, body, callback)</td>
    <td style="padding:15px">Updates an address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteById(addressId, callback)</td>
    <td style="padding:15px">Deletes an address by the address id</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insert(body, callback)</td>
    <td style="padding:15px">Creates an address</td>
    <td style="padding:15px">{base_path}/{version}/addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById1(alertId, callback)</td>
    <td style="padding:15px">Retrieves an alert by the alert id</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acknowledge(alertId, callback)</td>
    <td style="padding:15px">Acknowledges an alert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByAccount(callback)</td>
    <td style="padding:15px">Retrieves all alerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upload(body, callback)</td>
    <td style="padding:15px">Uploads a PDF and returns a list of details</td>
    <td style="padding:15px">{base_path}/{version}/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get1(filename, callback)</td>
    <td style="padding:15px">Retrieves a file, base64 encoded</td>
    <td style="padding:15px">{base_path}/{version}/assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search(auditEntity = 'ACCOUNT', auditAction = 'INSERT', targetId, node, callback)</td>
    <td style="padding:15px">Retrieves and audit log for the account</td>
    <td style="padding:15px">{base_path}/{version}/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById2(id, callback)</td>
    <td style="padding:15px">Retrieves a broker credit card by its id</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(id, callback)</td>
    <td style="padding:15px">Deletes a credit card</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCreditCardsByAccountId(callback)</td>
    <td style="padding:15px">Retrieves a credit card  by account id</td>
    <td style="padding:15px">{base_path}/{version}/credit_card?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update1(body, callback)</td>
    <td style="padding:15px">Updates a broker card</td>
    <td style="padding:15px">{base_path}/{version}/credit_card?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insert1(body, callback)</td>
    <td style="padding:15px">Creates a broker credit card</td>
    <td style="padding:15px">{base_path}/{version}/credit_card?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCreditCards(groupId, callback)</td>
    <td style="padding:15px">Gets all cards associated with a credit card group</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/group/{pathv1}/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">query(limit, sortDir = 'ASC', pageNumber, lastFourDigits, cardType = 'AMEX', nameOnCard, groupName, sortBy = 'CARD_TYPE', callback)</td>
    <td style="padding:15px">Retrieves all credit card groups</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(body, callback)</td>
    <td style="padding:15px">Updates a credit card group</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertGroup(body, callback)</td>
    <td style="padding:15px">Creates a credit card group</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupById(id, callback)</td>
    <td style="padding:15px">Retrieves a credit card group by its id</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(id, callback)</td>
    <td style="padding:15px">Deletes a credit card group</td>
    <td style="padding:15px">{base_path}/{version}/credit_card/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById3(customerId, callback)</td>
    <td style="padding:15px">Retrieves a customer by the customer id</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update4(customerId, body, callback)</td>
    <td style="padding:15px">Updates a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultsById(callback)</td>
    <td style="padding:15px">Retrieves default customers for the account</td>
    <td style="padding:15px">{base_path}/{version}/customers/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update3(body, callback)</td>
    <td style="padding:15px">Updates a default customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTag(customerId, tag, callback)</td>
    <td style="padding:15px">Deletes a tag for a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCards(customerId, cardId, callback)</td>
    <td style="padding:15px">Retrieves a card associated with a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/cards/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCard(customerId, cardId, callback)</td>
    <td style="padding:15px">Deletes a card associated with a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/cards/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCards1(customerId, callback)</td>
    <td style="padding:15px">Gets all cards associated with a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCard(customerId, body, callback)</td>
    <td style="padding:15px">Adds a credit card to a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultCard(customerId, cardId, callback)</td>
    <td style="padding:15px">Set a card associated with a customer as the default card</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/cards/{pathv2}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkRemoveTags(body, callback)</td>
    <td style="padding:15px">Deletes all tags from customers with given ids</td>
    <td style="padding:15px">{base_path}/{version}/customers/tags/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tag(customerId, body, callback)</td>
    <td style="padding:15px">Inserts a new tags for a customer. Duplicates are ignored</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update2(body, callback)</td>
    <td style="padding:15px">Bulk Customer Update</td>
    <td style="padding:15px">{base_path}/{version}/customers/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1(customerId, body, callback)</td>
    <td style="padding:15px">Deletes tags for a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers/{pathv1}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search1(limit, sortDir = 'ASC', pageNumber, customerType, tag, tagsMatchAll, antiTag, antiTagsMatchAll, id, displayName, email, lastName, company, city, country, phone, state, invoiceDateFrom, invoiceDateTo, sortedBy = 'ID', arThresholdReached, createdDateFrom, createdDateTo, callback)</td>
    <td style="padding:15px">Retrieves all customers</td>
    <td style="padding:15px">{base_path}/{version}/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insert2(body, callback)</td>
    <td style="padding:15px">Creates a customer</td>
    <td style="padding:15px">{base_path}/{version}/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumeration(enumtype = 'event', callback)</td>
    <td style="padding:15px">Retrieves available enumerations</td>
    <td style="padding:15px">{base_path}/{version}/enumerations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTag1(eventId, tag, callback)</td>
    <td style="padding:15px">Deletes a tag for an event</td>
    <td style="padding:15px">{base_path}/{version}/events/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkRemoveTags1(body, callback)</td>
    <td style="padding:15px">Deletes all tags from events with given ids</td>
    <td style="padding:15px">{base_path}/{version}/events/tags/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformers(name, callback)</td>
    <td style="padding:15px">Retrieves all performers</td>
    <td style="padding:15px">{base_path}/{version}/events/performers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformerById(performerId, callback)</td>
    <td style="padding:15px">Retrieves a performer by the performer id</td>
    <td style="padding:15px">{base_path}/{version}/events/performers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategories(callback)</td>
    <td style="padding:15px">Retrieves all categories</td>
    <td style="padding:15px">{base_path}/{version}/events/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tag1(eventId, body, callback)</td>
    <td style="padding:15px">Inserts a new tags for an event. Duplicates are ignored</td>
    <td style="padding:15px">{base_path}/{version}/events/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">index(limit, sortDir = 'ASC', pageNumber, tag, tagsMatchAll, antiTag, antiTagsMatchAll, event, eventType = 'CONCERT', categoryId, category, venueId, venue, city, state, eventDateFrom, eventDateTo, eventTimeFrom, eventTimeTo, keywords, eventId, performerId, dayOfWeek, excludeParking, excludeActiveInventory, noTags, sortedBy = 'ID', callback)</td>
    <td style="padding:15px">Retrieves filtered events</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get2(eventId, callback)</td>
    <td style="padding:15px">Returns an event</td>
    <td style="padding:15px">{base_path}/{version}/events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete2(eventId, body, callback)</td>
    <td style="padding:15px">Deletes tags for an event</td>
    <td style="padding:15px">{base_path}/{version}/events/{pathv1}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformers1(limit, sortDir = 'ASC', pageNumber, performerId, performer, eventType = 'CONCERT', categoryId, eventDateFrom, eventDateTo, dayOfWeek, excludeParking, sortedBy = 'NAME', callback)</td>
    <td style="padding:15px">Retrieves event positions grouped by performer</td>
    <td style="padding:15px">{base_path}/{version}/event_positions/performers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategories1(callback)</td>
    <td style="padding:15px">Retrieves event positions grouped by category</td>
    <td style="padding:15px">{base_path}/{version}/event_positions/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">index1(limit, sortDir = 'ASC', pageNumber, section, row, listed, eventId, event, keywords, eventDateFrom, eventDateTo, eventTimeFrom, eventTimeTo, dayOfWeek, eventType = 'CONCERT', categoryId, category, venueId, venue, city, state, performerId, tagsMatchAll, antiTagsMatchAll, inventoryTagsMatchAll, noTags, excludeOnlyZoneInventory, excludeParking, sortedBy = 'EVENT_DATE', callback)</td>
    <td style="padding:15px">Retrieves event positions</td>
    <td style="padding:15px">{base_path}/{version}/event_positions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById4(id, callback)</td>
    <td style="padding:15px">Get external account by id</td>
    <td style="padding:15px">{base_path}/{version}/external_accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update5(id, body, callback)</td>
    <td style="padding:15px">Update external accounts</td>
    <td style="padding:15px">{base_path}/{version}/external_accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete3(id, callback)</td>
    <td style="padding:15px">Delete external accounts</td>
    <td style="padding:15px">{base_path}/{version}/external_accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search2(callback)</td>
    <td style="padding:15px">Get all external accounts</td>
    <td style="padding:15px">{base_path}/{version}/external_accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(body, callback)</td>
    <td style="padding:15px">Creates an external account</td>
    <td style="padding:15px">{base_path}/{version}/external_accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById5(holdId, callback)</td>
    <td style="padding:15px">Retrieves a hold by the hold id</td>
    <td style="padding:15px">{base_path}/{version}/holds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update6(holdId, body, callback)</td>
    <td style="padding:15px">Updates a hold</td>
    <td style="padding:15px">{base_path}/{version}/holds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteById1(holdId, callback)</td>
    <td style="padding:15px">Deletes a hold by the hold id</td>
    <td style="padding:15px">{base_path}/{version}/holds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search3(limit, sortDir = 'ASC', pageNumber, id, inventoryId, exchangePosId, event, venueId, venue, user, userId, eventDateFrom, eventDateTo, createdDateFrom, createdDateTo, expiryDateFrom, expiryDateTo, externalRef, customerId, sortedBy = 'EVENT_NAME', callback)</td>
    <td style="padding:15px">Retrieves all holds</td>
    <td style="padding:15px">{base_path}/{version}/holds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insert3(body, callback)</td>
    <td style="padding:15px">Creates a hold</td>
    <td style="padding:15px">{base_path}/{version}/holds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventoryBarcodes(exchangePosId, callback)</td>
    <td style="padding:15px">Get barcodes for inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/barcodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventoryDeltas(startTime, endTime, ticketStatus = 'AVAILABLE', callback)</td>
    <td style="padding:15px">Get changed inventory (created, updated, deleted) by update time</td>
    <td style="padding:15px">{base_path}/{version}/inventory/delta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSold(limit, sortDir = 'ASC', pageNumber, sortedBy = 'EVENT_DATE', eventId, inventoryId, exchangePosId, invoiceId, purchaseIds, vendorId, eventType = 'CONCERT', categoryId, tag, tagsMatchAll, antiTag, antiTagsMatchAll, noTags, invoiceTag, invoiceTagsMatchAll, purchaseTag, purchaseTagsMatchAll, customerId, customerDisplayName, performerId, performer, eventKeywords, event, eventDateFrom, eventDateTo, venueId, venue, section, row, includesSeat, invoiceDateFrom, invoiceDateTo, currencyCode = 'USD', externalRef, purchaseExternalRef, paymentStatus, invoiceStatus, fulfillmentStatus = 'PENDING', stockType = 'HARD', zoneSeating, pdfsOrBarcodesAttached, filesUploaded, barcodesEntered, electronicTransfer, internalNotes, inHandDateFrom, inHandDateTo, fulfillmentDateFrom, fulfillmentDateTo, purchaseDateFrom, purchaseDateTo, cooperative, received, invoiceNotes, publicNotes, state, due, invoiceCreatedBy, createdByUserId, dayOfWeek, lastUpdateFrom, invoiceNotesUserId, consignment = 'YES', customerType = 'MARKETPLACE', excludeParking, callback)</td>
    <td style="padding:15px">Gets sold inventory filtered by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/inventory/sold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchPurchased(limit, sortDir = 'ASC', pageNumber, stockType = 'HARD', ticketStatus, eventId, inventoryId, performerId, vendorId, tag, tagsMatchAll, antiTag, antiTagsMatchAll, event, venue, city, state, section, row, purchaseDateFrom, purchaseDateTo, eventDateFrom, eventDateTo, externalRef, paymentStatus, partialPaymentRef, displayName, zoneSeating, notes, performer, purchaseId, inHandDateFrom, inHandDateTo, received, venueIds, consignment = 'YES', cooperative, sortedBy = 'EVENT_DATE', callback)</td>
    <td style="padding:15px">Gets purchased inventory filtered by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/inventory/purchased?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchPurchasedV2(limit, sortDir = 'ASC', pageNumber, stockType = 'HARD', ticketStatus, eventId, inventoryId, performerId, vendorId, tag, tagsMatchAll, antiTag, antiTagsMatchAll, event, venue, city, state, section, row, purchaseDateFrom, purchaseDateTo, eventDateFrom, eventDateTo, externalRef, paymentStatus, partialPaymentRef, displayName, zoneSeating, notes, performer, purchaseId, inHandDateFrom, inHandDateTo, received, venueIds, consignment = 'YES', cooperative, sortedBy = 'EVENT_DATE', creditCardGroupId, creditCardLastDigits, eventType = 'CONCERT', inHand, inventoryTag, inventoryTagsMatchAll, antiInventoryTag, antiInventoryTagsMatchAll, noTags, includesSeat, paymentMethod = 'CREDITCARD', minUnitCost, maxUnitCost, minTotalCost, maxTotalCost, createdBy, createdByUserId, currencyCode = 'USD', purchaseStatus, categoryId, callback)</td>
    <td style="padding:15px">Gets purchased inventory filtered by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/inventory/purchased/V2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHoldsById(inventoryId, callback)</td>
    <td style="padding:15px">Get holds for inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/hold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHoldsById(inventoryId, callback)</td>
    <td style="padding:15px">Delete holds for inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/hold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkMerge(body, callback)</td>
    <td style="padding:15px">Merge all eligible inventories</td>
    <td style="padding:15px">{base_path}/{version}/inventory/bulk-merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeAsPiggyback(body, callback)</td>
    <td style="padding:15px">Merge inventories as piggyback</td>
    <td style="padding:15px">{base_path}/{version}/inventory/merge-as-piggyback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitToOriginals(inventoryId, callback)</td>
    <td style="padding:15px">Splits and merges inventories to leave them as they were when purchased</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/actions/split-to-originals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById6(inventoryId, callback)</td>
    <td style="padding:15px">Get Inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update7(inventoryId, changeSeatNumbers, body, callback)</td>
    <td style="padding:15px">Update Inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">priceHistory(inventoryId, callback)</td>
    <td style="padding:15px">Retrieves price update history for inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/price-history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkSwapEvent(body, callback)</td>
    <td style="padding:15px">Bulk swap event</td>
    <td style="padding:15px">{base_path}/{version}/inventory/bulk-swap-event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExchangesPosId(exchangePosId, callback)</td>
    <td style="padding:15px">Get the history for a exchange pos id</td>
    <td style="padding:15px">{base_path}/{version}/inventory/exchange_pos_id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkUpdate(body, callback)</td>
    <td style="padding:15px">Bulk update inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/bulk-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTag2(inventoryId, tag, callback)</td>
    <td style="padding:15px">Deletes a tag for an inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkRemoveTags2(body, callback)</td>
    <td style="padding:15px">Deletes all tags from inventories with given ids</td>
    <td style="padding:15px">{base_path}/{version}/inventory/tags/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">legacyBulkRemoveTags(id, callback)</td>
    <td style="padding:15px">Deletes all tags from inventories with given ids</td>
    <td style="padding:15px">{base_path}/{version}/inventory/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePurchaseInventory(body, callback)</td>
    <td style="padding:15px">Updates section and row for an inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/actions/edit-section-row?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkUpdateExpectedValue(body, callback)</td>
    <td style="padding:15px">Bulk Expected Value Update of Inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/bulk-update-expected-value?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketsById(inventoryId, callback)</td>
    <td style="padding:15px">Get tickets for inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTickets(inventoryId, body, callback)</td>
    <td style="padding:15px">Update Inventory tickets</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancel(inventoryId, callback)</td>
    <td style="padding:15px">Cancel Tickets on Inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">export(includeZeroPricedListings, includeHeld, callback)</td>
    <td style="padding:15px">Exports inventory for exchange integration. Export is limited to future events with a list price greater than 0. The results from this method are un-sorted.</td>
    <td style="padding:15px">{base_path}/{version}/inventory/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tag2(inventoryId, body, callback)</td>
    <td style="padding:15px">Inserts a new tags for an inventory. Duplicates are ignored</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search4(limit, sortDir = 'ASC', pageNumber, sortedBy = 'EVENT_DATE', stockType = 'HARD', ticketStatus, eventId, inventoryId, purchaseId, exchangePosId, performerId, categoryId, tag, tagsMatchAll, antiTag, antiTagsMatchAll, eventTagsMatchAll, event, eventKeywords, venueId, venue, vendorId, excludeVendorId, city, state, section, row, eventDateFrom, eventDateTo, eventTimeFrom, eventTimeTo, eventType = 'CONCERT', purchaseDateFrom, purchaseDateTo, lastUpdateFrom, lastUpdateTo, lastPriceUpdateFrom, lastPriceUpdateTo, sortedByB = 'ID', sortDirB = 'ASC', filesUploaded, barCodesEntered, listed, expectedValueSet, skipSorting, minQuantity, maxQuantity, minPrice, maxPrice, minAverageUnitCost, maxAverageUnitCost, lowSeat, highSeat, inHand, notes, publicNotes, noTags, includeTickets, zoneSeating, dayOfWeek, seatType = 'CONSECUTIVE', splitType = 'DEFAULT', noSplits, currencyCode = 'USD', hiddenSeats, cooperative, electronicTransfer, vsrOption = 'ALL', replenishmentGroupId, consignment = 'YES', received, onHold, daysOldFrom, daysOldTo, excludeParking, callback)</td>
    <td style="padding:15px">Get inventory filtered by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update8(body, callback)</td>
    <td style="padding:15px">Bulk Price Update of Inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">split(inventoryId, body, callback)</td>
    <td style="padding:15px">Splits inventory into 2 new inventory groups</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">split1(inventoryId, callback)</td>
    <td style="padding:15px">Splits inventory into 2 new inventory consecutive groups</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/split-to-consecutive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete4(inventoryId, body, callback)</td>
    <td style="padding:15px">Deletes tags for an inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/{pathv1}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">merge(body, callback)</td>
    <td style="padding:15px">Merge the list of inventories into a new inventory</td>
    <td style="padding:15px">{base_path}/{version}/inventory/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sync(body, callback)</td>
    <td style="padding:15px">Sync Inventory with integrated services</td>
    <td style="padding:15px">{base_path}/{version}/inventory/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceById(invoiceId, callback)</td>
    <td style="padding:15px">Retrieves an invoice by the invoice id</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInvoice(invoiceId, body, callback)</td>
    <td style="padding:15px">Updates an invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceTicketsByExternalRef(externalRef, callback)</td>
    <td style="padding:15px">Returns tickets on an invoice by external ref</td>
    <td style="padding:15px">{base_path}/{version}/invoices/external-ref/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceTicketsByExternalRefV2(externalRef, callback)</td>
    <td style="padding:15px">Returns tickets on an invoice by external ref</td>
    <td style="padding:15px">{base_path}/{version}/invoices/external-ref?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceLines(invoiceId, callback)</td>
    <td style="padding:15px">Gets invoice lines</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/lines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertInvoiceLine(invoiceId, body, callback)</td>
    <td style="padding:15px">Adds an invoice line</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/lines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceLine(invoiceId, lineId, callback)</td>
    <td style="padding:15px">Gets an invoice line</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/lines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInvoiceLine(invoiceId, lineId, body, callback)</td>
    <td style="padding:15px">Updates an invoice line. Only quantity, amount, and description can be updated.</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/lines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInvoiceLine(invoiceId, lineId, callback)</td>
    <td style="padding:15px">Deletes an invoice line. Tickets are returned back to the inventory pool.</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/lines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceLineTickets(invoiceId, lineId, callback)</td>
    <td style="padding:15px">Get tickets for inventory</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/lines/{pathv2}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssets(invoiceId, callback)</td>
    <td style="padding:15px">Gets invoice assets</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search5(limit, sortDir = 'ASC', pageNumber, deliveryMethod, tag, tagsMatchAll, antiTag, antiTagsMatchAll, displayName, vendorId, id, createdDateFrom, createdDateTo, lastUpdateFrom, lastUpdateTo, externalRef, paymentStatus, paymentDateFrom, paymentDateTo, invoiceStatus, fulfillmentStatus = 'PENDING', fulfillmentDateFrom, fulfillmentDateTo, paymentRef, barcodesEntered, filesUploaded, zoneSeating, performerId, performer, inHandDateFrom, inHandDateTo, eventDateFrom, eventDateTo, customerId, eventType = 'CONCERT', categoryId, createdBy, createdByUserId, noTags, currencyCode = 'USD', eventId, internalId, purchaseId, purchaseLineId, sortedBy = 'ID', includeTransactionInfo, received = 'YES', venueId, venue, stockType = 'HARD', minTotal, maxTotal, invoiceNotesUserId, callback)</td>
    <td style="padding:15px">Searches invoices</td>
    <td style="padding:15px">{base_path}/{version}/invoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertInvoice(force, body, callback)</td>
    <td style="padding:15px">Creates an invoice. The minimum fields required for creating an invoices are customerId, lines, line.amount, line.itemIds, line.lineItemType.</td>
    <td style="padding:15px">{base_path}/{version}/invoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTag3(invoiceId, tag, callback)</td>
    <td style="padding:15px">Deletes a tag for an invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkRemoveTags3(body, callback)</td>
    <td style="padding:15px">Deletes all tags from invoices with given ids</td>
    <td style="padding:15px">{base_path}/{version}/invoices/tags/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processPayment(invoiceId, body, callback)</td>
    <td style="padding:15px">Process a payment for a invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/payment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransactionHistory(invoiceId, callback)</td>
    <td style="padding:15px">List all transactions appearing on an invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/transactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processRefund(invoiceId, transactionId, body, callback)</td>
    <td style="padding:15px">Refunds a payment to the card used</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/transactions/{pathv2}/refund?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">printCustomAuthForm(invoiceId, timeZoneOffset, callback)</td>
    <td style="padding:15px">Prints an Invoice Custom Auth Form</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/print-auth-form?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCurrency(body, callback)</td>
    <td style="padding:15px">Converts invoice currency using an exchange rate</td>
    <td style="padding:15px">{base_path}/{version}/invoices/actions/update-invoice-currency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">send(invoiceId, emailAddress, ccEmailAddress, bccEmailAddress, subject, message, includeInvoice, attachTickets, attachInvoicePDF, attachAuthForm, timeZoneOffset, callback)</td>
    <td style="padding:15px">Sends an Invoice to the customer's email address</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/send?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tag3(invoiceId, body, callback)</td>
    <td style="padding:15px">Inserts a new tags for an invoice. Duplicates are ignored</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update9(body, callback)</td>
    <td style="padding:15px">Bulk Invoice Update</td>
    <td style="padding:15px">{base_path}/{version}/invoices/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete5(invoiceId, body, callback)</td>
    <td style="padding:15px">Deletes tags for an invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">print(invoiceId, timeZoneOffset, callback)</td>
    <td style="padding:15px">Prints an Invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}/print?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLineById(lineId, callback)</td>
    <td style="padding:15px">Retrieves a line by the line id</td>
    <td style="padding:15px">{base_path}/{version}/lines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDelete(body, callback)</td>
    <td style="padding:15px">Deletes a bulk of mappings and cancels the purchase lines they are associated with</td>
    <td style="padding:15px">{base_path}/{version}/mappings/actions/bulk-cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findPendingById(mappingId, callback)</td>
    <td style="padding:15px">Returns a mapping</td>
    <td style="padding:15px">{base_path}/{version}/mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete6(mappingId, callback)</td>
    <td style="padding:15px">Deletes a mapping and cancels the purchase line it's associated with</td>
    <td style="padding:15px">{base_path}/{version}/mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPendingByAccount(purchaseId, externalRef, event, venue, callback)</td>
    <td style="padding:15px">Gets all pending mappings</td>
    <td style="padding:15px">{base_path}/{version}/mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resendMappingRequest(mappingId, callback)</td>
    <td style="padding:15px">Re-sends a mapping to the mapping queue. Returns the mapping id</td>
    <td style="padding:15px">{base_path}/{version}/mappings/{pathv1}/resend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTag4(purchaseId, tag, callback)</td>
    <td style="padding:15px">Deletes a tag for a purchase</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkRemoveTags4(body, callback)</td>
    <td style="padding:15px">Deletes all tags from purchases with given ids</td>
    <td style="padding:15px">{base_path}/{version}/purchases/tags/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseById(purchaseId, callback)</td>
    <td style="padding:15px">Retrieves a purchase by the purchase id</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePurchase(purchaseId, body, callback)</td>
    <td style="padding:15px">Updates a purchase</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePurchaseInventory1(purchaseId, lineId, body, callback)</td>
    <td style="padding:15px">Updates section and row for a purchase line</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines/{pathv2}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAirbill(purchaseId, callback)</td>
    <td style="padding:15px">Retrieves a purchase's airbill</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/airbill?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCurrency1(body, callback)</td>
    <td style="padding:15px">Converts purchases currency using an exchange rate</td>
    <td style="padding:15px">{base_path}/{version}/purchases/actions/update-purchase-currency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAutoPurchases(limit, sortDir = 'ASC', pageNumber, eventId, purchaseId, user, userId, externalRef, fromPurchaseDate, toPurchaseDate, eventName, venueId, venue, ticketStatus = 'AVAILABLE', sortedBy = 'ID', callback)</td>
    <td style="padding:15px">Searches auto purchases</td>
    <td style="padding:15px">{base_path}/{version}/purchases/auto-purchases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPurchaseNote(purchaseId, body, callback)</td>
    <td style="padding:15px">Adds a purchase note</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search6(limit, sortDir = 'ASC', pageNumber, deliveryMethod, paymentMethod, vendorType, tag, tagsMatchAll, antiTag, antiTagsMatchAll, vendorTag, vendorTagsMatchAll, vendorId, displayName, id, inventoryId, invoiceLineId, creditCardId, createdDateFrom, createdDateTo, lastUpdateFrom, lastUpdateTo, externalRef, simplifiedExternalRef, paymentStatus, purchaseStatus, minCost, maxCost, currencyCode = 'USD', internalId, sortedBy = 'ID', includeHasZoneSeatingInventory, zeroOutstandingBalance, minOutstandingBalance, maxOutstandingBalance, consignment = 'YES', eventId, eventName, cooperative, createdBy, createdByUserId, lastPurchasePaymentNote, vendorTags, callback)</td>
    <td style="padding:15px">Searches purchases</td>
    <td style="padding:15px">{base_path}/{version}/purchases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPurchase(body, callback)</td>
    <td style="padding:15px">Creates a purchase. The minimum required field to insert a purchase is the vendorId. Inventory purchase lines must include quantity, section, row, cost, lowSeat, highSeat, stockType,  seatType and either an eventId or eventMapping.</td>
    <td style="padding:15px">{base_path}/{version}/purchases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseLines(purchaseId, callback)</td>
    <td style="padding:15px">Gets purchase lines</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPurchaseLine(purchaseId, body, callback)</td>
    <td style="padding:15px">Adds a purchase line</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseLine(purchaseId, lineId, callback)</td>
    <td style="padding:15px">Gets a purchase line</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePurchaseLine(purchaseId, lineId, body, callback)</td>
    <td style="padding:15px">Updates a purchase line. Only amount and description can be updated.</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replacePurchaseLine(purchaseId, lineId, body, callback)</td>
    <td style="padding:15px">Replaces a purchase line. Allows changing section, row, low seat and seat type.</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines/{pathv2}/actions/replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseLineTickets(purchaseId, lineId, callback)</td>
    <td style="padding:15px">Gets the tickets of a purchase line</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines/{pathv2}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelPurchaseLine(purchaseId, lineId, callback)</td>
    <td style="padding:15px">Cancels a purchase line</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/lines/{pathv2}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiWithPurchaseId(purchaseId, body, callback)</td>
    <td style="padding:15px">Creates inventory</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">send1(purchaseId, emailAddress, ccEmailAddress, bccEmailAddress, subject, message, includeAirbill, timeZoneOffset, callback)</td>
    <td style="padding:15px">Sends a Purchase Order to the vendor's email address</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/send?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tag4(purchaseId, body, callback)</td>
    <td style="padding:15px">Inserts a new tags for a purchase. Duplicates are ignored.</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update10(body, callback)</td>
    <td style="padding:15px">Bulk Purchase Update</td>
    <td style="padding:15px">{base_path}/{version}/purchases/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete7(purchaseId, body, callback)</td>
    <td style="padding:15px">Deletes tags for a purchase</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">print1(purchaseId, timeZoneOffset, callback)</td>
    <td style="padding:15px">Prints a Purchase Order</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/print?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search7(purchaseId, callback)</td>
    <td style="padding:15px">Retrieves a single purchase's audit records</td>
    <td style="padding:15px">{base_path}/{version}/purchases/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search8(key, callback)</td>
    <td style="padding:15px">Searches by the passed criteria</td>
    <td style="padding:15px">{base_path}/{version}/quicksearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryReportSnapshots(reportId, snapshotDateFrom, snapshotDateTo, callback)</td>
    <td style="padding:15px">Searches snapshots</td>
    <td style="padding:15px">{base_path}/{version}/reports/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportSnapshot(id, callback)</td>
    <td style="padding:15px">Retrieves a snapshot by the snapshot id</td>
    <td style="padding:15px">{base_path}/{version}/reports/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulk(body, callback)</td>
    <td style="padding:15px">Bulk add/remove tags</td>
    <td style="padding:15px">{base_path}/{version}/tags/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagsSummary(limit, sortDir = 'ASC', pageNumber, id, tag, callback)</td>
    <td style="padding:15px">Tags summary</td>
    <td style="padding:15px">{base_path}/{version}/tags/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete8(body, callback)</td>
    <td style="padding:15px">Deletes tags for an account</td>
    <td style="padding:15px">{base_path}/{version}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">list(callback)</td>
    <td style="padding:15px">Retrieves a list of tags for an account</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search9(barcode, lastUpdateFrom, lastUpdateTo, eventId, section, row, seatNumber, callback)</td>
    <td style="padding:15px">Searches a ticket with the given barcode</td>
    <td style="padding:15px">{base_path}/{version}/tickets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsers(callback)</td>
    <td style="padding:15px">Gets a list of users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMe(callback)</td>
    <td style="padding:15px">Gets the current user details</td>
    <td style="padding:15px">{base_path}/{version}/users/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById7(vendorId, callback)</td>
    <td style="padding:15px">Retrieves a vendor by the vendor id</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update12(vendorId, body, callback)</td>
    <td style="padding:15px">Updates a vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTag5(vendorId, tag, callback)</td>
    <td style="padding:15px">Deletes a tag for a vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkRemoveTags5(body, callback)</td>
    <td style="padding:15px">Deletes all tags from vendors with given ids</td>
    <td style="padding:15px">{base_path}/{version}/vendors/tags/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefault(callback)</td>
    <td style="padding:15px">Retrieves the default vendor for the account</td>
    <td style="padding:15px">{base_path}/{version}/vendors/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDefaultVendor(body, callback)</td>
    <td style="padding:15px">Sets the default vendor for the account</td>
    <td style="padding:15px">{base_path}/{version}/vendors/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tag5(vendorId, body, callback)</td>
    <td style="padding:15px">Inserts a new tags for a vendor. Duplicates are ignored</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update11(body, callback)</td>
    <td style="padding:15px">Bulk Vendor Update</td>
    <td style="padding:15px">{base_path}/{version}/vendors/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete9(vendorId, body, callback)</td>
    <td style="padding:15px">Deletes tags for a vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}/tags/actions/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search10(limit, sortDir = 'ASC', pageNumber, id, vendorType, tag, tagsMatchAll, antiTag, antiTagsMatchAll, displayName, email, lastName, company, city, state, phone, sortedBy = 'DISPLAY_NAME', callback)</td>
    <td style="padding:15px">Retrieves all vendors</td>
    <td style="padding:15px">{base_path}/{version}/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insert4(body, callback)</td>
    <td style="padding:15px">Creates vendors</td>
    <td style="padding:15px">{base_path}/{version}/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById8(venueId, callback)</td>
    <td style="padding:15px">Retrieves a venue by the venue id</td>
    <td style="padding:15px">{base_path}/{version}/venues/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search11(limit, sortDir = 'ASC', pageNumber, name, city, state, id, sortedBy = 'NAME', callback)</td>
    <td style="padding:15px">Searches venues</td>
    <td style="padding:15px">{base_path}/{version}/venues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">find(id, callback)</td>
    <td style="padding:15px">Get Webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update13(id, body, callback)</td>
    <td style="padding:15px">Update Webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete10(id, callback)</td>
    <td style="padding:15px">Delete Webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">query1(callback)</td>
    <td style="padding:15px">Get all Webhooks created</td>
    <td style="padding:15px">{base_path}/{version}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create1(body, callback)</td>
    <td style="padding:15px">Create Webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
